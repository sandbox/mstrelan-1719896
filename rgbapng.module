<?php

/**
 * Implements hook_menu().
 */
function rgbapng_menu() {
  $items = array();
  $directory_path = file_stream_wrapper_get_instance_by_scheme('public')->getDirectoryPath();
  $items[$directory_path . '/rgbapng/%'] = array(
    'title' => 'RGBa PNG',
    'page callback' => 'rgbapng_image',
    'page arguments' => array(1),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  return $items;
}

function rgbapng_image($hex) {

  // Strip .png from the hex value if it exists.
  if (strripos($hex, '.png') !== FALSE) {
    $hex = str_ireplace('.png', '', $hex);
  }

  // Get the final URI the image will use
  $uri = 'public://rgbapng/' . check_plain($hex) . '.png';

  // Don't start generating the image if it already exists or if generation is
  // in progress in another thread.
  $lock_name = 'rgbapng:' . drupal_hash_base64($uri);

  if (!file_exists($uri)) {
    $lock_acquired = lock_acquire($lock_name);
    if (!$lock_acquired) {
      // Tell client to retry again in 3 seconds. Currently no browsers are known
      // to support Retry-After.
      drupal_add_http_header('Status', '503 Service Unavailable');
      drupal_add_http_header('Retry-After', 3);
      print t('Image generation in progress. Try again shortly.');
      drupal_exit();
    }
  }


  $success = file_exists($uri) || rgbapng_create_image($hex, $uri);

  if (!empty($lock_acquired)) {
    lock_release($lock_name);
  }

  if ($success) {
    $image = image_load($uri);
    file_transfer($image->source, array('Content-Type' => $image->info['mime_type'], 'Content-Length' => $image->info['file_size']));
  }


  else {
    return $uri;
  }
}

function rgbapng_create_image($hex, $uri = FALSE) {

  if (!$uri) {
    $uri = 'public://rgbapng/' . check_plain($hex);
  }

  $directory = drupal_dirname($uri);

  // Build the destination folder tree if it doesn't already exist.
  if (!file_prepare_directory($directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
    watchdog('rgpapng', 'Failed to create rgapng directory: %directory', array('%directory' => $directory), WATCHDOG_ERROR);
    return FALSE;
  }

  // image_get_available_toolkits();

  list($r, $g, $b, $a) = rgbapng_hex2rgba($hex);
  $image = new stdClass();
  $image->toolkit = image_get_toolkit();
  $image->info = array(
    'width' => 1,
    'height' => 1,
    'extension' => 'png',
    'mime_type' => 'image/png',
  );
  $image->resource = @imagecreatetruecolor(1, 1)
    or die('Cannot Initialize new GD image stream');

  $color = imagecolorallocatealpha($image->resource, $r, $g, $b, $a);
  imagefill($image->resource, 0, 0, $color);
  imagealphablending($image->resource, FALSE);
  imagesavealpha($image->resource, TRUE);


  if (!image_save($image, $uri)) {
    if (file_exists($uri)) {
      watchdog('rgbapng', 'Cached image file %destination already exists. There may be an issue with your rewrite configuration.', array('%destination' => $destination), WATCHDOG_ERROR);
    }
    return FALSE;
  }

  return TRUE;

}

function rgbapng_hex2rgba($hex) {
  $hex = ltrim($hex, '#');
  if (preg_match('/^[0-9a-f]{3}$/i', $hex)) {
    // 'FA3' is the same as 'FFAA33' so r=FF, g=AA, b=33
    $r = str_repeat($hex{0}, 2);
    $g = str_repeat($hex{1}, 2);
    $b = str_repeat($hex{2}, 2);
    $a = '0';
  }
  elseif (preg_match('/^[0-9a-f]{6}$/i', $hex)) {
    // #FFAA33 or r=FF, g=AA, b=33
    list($r, $g, $b) = str_split($hex, 2);
    $a = '0';
  }
  elseif (preg_match('/^[0-9a-f]{8}$/i', $hex)) {
    // #FFAA33 or r=FF, g=AA, b=33
    list($r, $g, $b, $a) = str_split($hex, 2);
  }
  elseif (preg_match('/^[0-9a-f]{4}$/i', $hex)) {
    // 'FA37' is the same as 'FFAA3377' so r=FF, g=AA, b=33, a=77
    $r = str_repeat($hex{0}, 2);
    $g = str_repeat($hex{1}, 2);
    $b = str_repeat($hex{2}, 2);
    $a = str_repeat($hex{3}, 2);
  }
  else {
    //error: invalide hex string, TODO: set form error..
    return false;
  }

  $r = hexdec($r);
  $g = hexdec($g);
  $b = hexdec($b);
  $a = hexdec($a);
  return array($r, $g, $b, $a);
}
